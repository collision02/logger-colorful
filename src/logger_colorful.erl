-module(logger_colorful).

%% API exports
-export([
    check_config/1,
    format/2
]).

check_config(Config) ->
    logger_formatter:check_config(Config).

format(#{level := Level, meta := Meta} = LoggingEvent, Config) ->
    Meta0 = Meta#{file => filename:basename(maps:get(file, Meta, ""))},
    unicode:characters_to_binary([
        color(Level, logger_formatter:format(LoggingEvent#{meta => Meta0}, Config))
    ]).

color(emergency, Str)   -> color:on_red(color:yellow(Str));
color(alert, Str)       -> color:on_red(color:white(Str));
color(critical, Str)    -> color:on_black(color:yellow(Str));
color(error, Str)       -> color:red(Str);
color(warning, Str)     -> color:yellow(Str);
color(notice, Str)      -> color:cyan(Str);
color(info, Str)        -> color:white(Str);
color(_, Str)           -> Str.